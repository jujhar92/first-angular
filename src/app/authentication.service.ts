import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthenticationService {

  public isLoggedIn:boolean = false;
  constructor(private http:HttpClient) { }

  isAuthenticated(userID:Number,userName:String,password:String){
    const body = {
      id:userID,
      userName :userName,
      password:password
    };
    
    return this.http.post<{isAuthenticated:boolean,message:String,statusCode:Number}>('http://localhost:8080/SpringMVC/controller/authenticate',body,{observe:'response'});
  }
}
