import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  loginForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthenticationService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userID:['',Validators.required],
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  loginUser(e) {
    this.authService.isAuthenticated(this.loginForm.controls.userID.value,this.loginForm.controls.userName.value, this.loginForm.controls.password.value).subscribe(res => {
      if (res.body.isAuthenticated) {
        this.authService.isLoggedIn = true;
        this.router.navigate(['dashboard']);
      }
      else {
        this.authService.isLoggedIn = false;
        this.router.navigate(['']);
      }
    }, err => {
      this.authService.isLoggedIn = false;
      this.router.navigate(['']);
      console.error(err.error);
    });
  }

}
