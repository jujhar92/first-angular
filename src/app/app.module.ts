import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule,Routes} from '@angular/router';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';
import { PageNotFoundComponent} from './page-not-found/page-not-found.component';
import { AuthenticationService } from './authentication.service';
import { AuthGuard } from './auth.guard';
import {BasicInterceptor} from './basic-interceptor';

const appRoutes:Routes = [
  {
    path:'',
    component:LoginFormComponent
  },
  {
    path:'dashboard',
    component:HomeDashboardComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'**',
    component:PageNotFoundComponent
  }
];
@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    HeaderComponent,
    FooterComponent,
    HomeDashboardComponent,
    PageNotFoundComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [AuthenticationService,AuthGuard,{
    provide: HTTP_INTERCEPTORS,
    useClass: BasicInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
