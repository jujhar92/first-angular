const express = require('express');
const app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.post('/authenticate', function (req, res) {
    if (req.body.userName == "jujhar" && req.body.password) {
        res.send({authenticated:true});
    }
    else {
        res.send({authenticated:false});
    }
});
app.get('/serverStatus', function (req, res) {
    res.send('Hello,Thanks for checking , server is up and running at localhost:4201');
});

app.listen(4201);
console.log("node server started at localhost:4201");